
use convenio;

drop procedure if exists ro_account_list_for_user;

delimiter $$

create procedure ro_account_list_for_user(
  IN inUserId integer
  )
BEGIN
  declare clientAccountId integer unsigned;

  select client_account_id into clientAccountId from tblUsers where id=inUserId;

  select
    MA.id,
    MA.nickname,
    MAT.name accountTypeName,
    if(MA.enabled='Y','Yes','No') as enabled,
    if(MA.sandbox_account='Y','Yes','No') as sandbox_account,
    sum(if(MAB.id is null,0,1)) as batch_count
  from
    tblMerchantAccounts MA left join
    tblMerchantAccountTypes MAT on MA.type_id=MAT.id left join
    tblMerchantAccountBatches MAB on MA.id=MAB.account_id
  where
    MA.client_account_id = clientAccountId
  group by
    MA.id
  order by
    MA.nickname;

END;

$$

DELIMITER ;
