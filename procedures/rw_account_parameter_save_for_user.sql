
use convenio;

drop procedure if exists rw_account_parameter_save_for_user;

delimiter $$

create procedure rw_account_parameter_save_for_user(
  IN inUserId integer,
  IN inAccountId integer,
  IN inParameterId integer,
  IN inParameterCode varchar(64),
  IN inValue varchar(255)
  )
BEGIN
  declare accountTypeId integer;
  declare paramId integer;

  select id into paramId from tblMerchantAccountParameterTypes where param_code=inParameterCode;

  if inParameterId = -1 then
    select id into inParameterId from tblMerchantAccountParameters where account_id=inAccountId and param_id=paramId;
  end if;
  if ifnull(inParameterId,-1) = -1 then
    insert into
      tblMerchantAccountParameters (
        id, account_id, param_id, param_value
      )
      values(
        null,
        inAccountId,
        paramId,
        aes_encrypt(inValue,key_db.f_key())
      );
    select last_insert_id() into inParameterId;
  else
    update
      tblMerchantAccountParameters
    set
      param_value = aes_encrypt(inValue,key_db.f_key())
    where
      id = inParameterId and
      account_id = inAccountId;
  end if;

  select inParameterId as parameter_id;
END;

$$

DELIMITER ;
