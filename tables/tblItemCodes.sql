
-- 16 * (trunc(string_length / 16) + 1)

use convenio;

drop table if exists tblItemCodes;

create table tblItemCodes (
  id integer unsigned not null primary key auto_increment,
  client_account_id integer unsigned,
  merchant_account_id integer unsigned,  -- if zero, is global for client
  item_code varchar(32),
  enabled char(1) default 'Y',
  create_datetime timestamp(6) default CURRENT_TIMESTAMP(6),
  update_datetime timestamp(6) default CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  index i_item_code(item_code)
) engine=InnoDB default charset=utf8;

