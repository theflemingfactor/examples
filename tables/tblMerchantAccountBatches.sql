
-- 16 * (trunc(string_length / 16) + 1)

use convenio;

drop table if exists tblMerchantAccountBatches;

create table tblMerchantAccountBatches (
  id integer unsigned not null primary key auto_increment,
  account_id integer unsigned,
  type_id smallint unsigned,  -- anet etc
  batch_code varchar(32),
  settled_total decimal(14,2),
  settle_datetime timestamp(6) default null,
  settle_status varchar(32),
  details_retrieved char(1),
  create_datetime timestamp(6) default CURRENT_TIMESTAMP(6),
  update_datetime timestamp(6) default CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  index i_account_id(account_id),
  index i_batch_code(batch_code),
  index i_type_id(type_id),
  index i_details_retrieved(details_retrieved)
) engine=InnoDB default charset=utf8;

