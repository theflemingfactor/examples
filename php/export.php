<?php

/*
 * Takes web post request with json formatted data array and json formatting array and generates Excel spreadsheet
 *
 * Author: Peter Fleming
 *
 */
session_start();
date_default_timezone_set('America/Chicago');
setlocale(LC_MONETARY, 'en_US');
$secondsInADay = 86400;
$unixTimeToExcelDateDiff = $secondsInADay * 25569;

set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . '/../private/'  . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . '/../private/classes/' );
include 'classes/Spreadsheet/Excel/Writer.php';

include_once('modules/exportFormatArray.php');

$fieldNameAdjuster = (isset($_POST['fieldNameAdjuster']) ? $_POST['fieldNameAdjuster'] : '');
$colFormatArray = json_decode($_POST['txtJSONFormat'.$fieldNameAdjuster]);
$transArray = json_decode($_POST['txtJSON'.$fieldNameAdjuster]);

// start of excel file creation
$workbook = new SpreadSheet_Excel_Writer();
$workbook->send('Export' . rand(0,100000) . '.xls');
$worksheet =& $workbook->addWorksheet("Export");
$workbook->setVersion(8);
$worksheet->setInputEncoding('UTF-8');
$formatArray = buildExportFormatArray($workbook);

// Calc cell widths
$columnWidth = array();
foreach ($transArray as $rowArray) {
    $column = 0;
    foreach ($rowArray as $key => $columnFormat) {
        $cellValue = trim(strip_tags(str_ireplace('<BR>',' ' ,(string)$rowArray->$key)));
        error_log($key.':: '.strlen($cellValue).': '.$cellValue);
        if(!isset($columnWidth[$key])) $columnWidth[$key] = 0;
        $columnWidth[$key] = max(strlen($cellValue),$columnWidth[$key]);
        $column++;
        }
    }

$columnKeys = array();
$row = 0;
foreach ($colFormatArray as $rowFormat) {
    $column = 0;
    foreach ($rowFormat as $key => $cellFormat) {
        $columnKeys[$key] = $cellFormat;
        $format = (isset($cellFormat->format) ? $cellFormat->format : '');
        $headerValue = (string)$cellFormat->label;
        $headerWidth = strlen(strstr($headerValue.PHP_EOL,PHP_EOL,true));
        $headerValue = str_ireplace('<BR>',PHP_EOL,$headerValue);
        switch($format) {
            case 'currency':
                $worksheet->setColumn($row, $column,max($columnWidth[$key],$headerWidth,10));
                $worksheet->write($row, $column,$headerValue, $formatArray['boldRight']);
                break;
            default:
                $worksheet->setColumn($row, $column,max($columnWidth[$key],$headerWidth,10));
                $worksheet->write($row, $column, $headerValue, $formatArray['bold']);
            }
        $column++;
        }
    }
$row++;

foreach ($transArray as $rowArray) {
    $cell = 0;
    //foreach ($rowArray as $cellItem) {
    foreach ($columnKeys as $key => $columnFormat) {
        $formatName = (isset($columnFormat->format) ? $columnFormat->format : '');
        $format = ($formatName ? $formatArray[$columnFormat->format] : '');
        $cellValue = trim(strip_tags(str_ireplace('<BR>',' ' ,(string)$rowArray->$key)));
        // Hide SSN
        if(strtolower($key) == 'ssn') {
            if(strlen(trim($cellValue)) >=4 ) {
                $cellValue = 'xxx-xx-' . substr($cellValue,-4);
                }
            else{
                $cellValue = '';
                }
            }
        if($format) {
            switch($columnFormat->format) {
                case 'currency':
                    $worksheet->write($row, $cell, 0+str_replace(',','',str_replace('$','',$cellValue)),$format);
                    break;
                case 'date':
                    if($cellValue) {
                        $excelDate = (strtotime($cellValue) + $unixTimeToExcelDateDiff) / $secondsInADay;
                        $worksheet->write($row, $cell,$excelDate,$format);
                        }
                    break;
                default:
                    $worksheet->write($row, $cell, $cellValue,$format);
                }
            }
        else {
            $worksheet->writeString($row, $cell, $cellValue);
            }
        $cell++;
        }
    $row++;
    }

/*
for($j = 0; $j < sizeof($transArray); $j++) {
        for ($i = 0; $i < $size; $i++) {
            error_log("$row : $i " . $transArray[$j][$i]);
            if ($i != 6) {
                $worksheet->write($row, $i, $transArray[$j][$i]);
            }
            else {
                $worksheet->write($row, $i, $transArray[$j][$i], $currencyFormat);
            }
        }   
        $row++;
    }
    $worksheet->writeFormula($row, 6, '=SUM(G1:G' . $row . ')', $currencyFormat);
*/
    $workbook->close();    
    
// }
