<?php

/*
 * Static calls for MySQL access
 */

define('mysqliConn_Logger_Category','mysqli');

include_once(dirname(__FILE__) . '/Logger.class.php');
include_once(dirname(__FILE__) . '/Config.class.php');

class mysqliConn {
   private static $MySQLConn = array();
   private static $MySQLDbf = '';
   private static $LogQueries=false;
   private static $lastRowsReturned = 0;

   private function connect() {
      //session_set_cookie_params(525600);
      self::$MySQLConn = mysqli_connect(Config::getConfig('MySQL/host'),Config::getConfig('MySQL/username'),Config::getConfig('MySQL/password'));
      if (!self::$MySQLConn) {
         Logger::LogIt('Error: Unable to connect with database server.',Logger_ERRLVL_DEBUG,mysqliConn_Logger_Category);
         if(Config::getConfig('MySQL/emailerrorto')) {
            mail(Config::getConfig('MySQL/emailerrorto'),$_SERVER['HTTP_HOST'] . ' Unable to connect with database server ', 'PHP_SELF=' . $_SERVER['PHP_SELF'] . "\nURI=" . $_SERVER['REQUEST_URI'] . "\nFile=" . __file__ . "\nLine#=" . __line__ . "\nIP=" . $_SERVER['REMOTE_ADDR'] . "\n" . $_SERVER['HTTP_REFERER'] . "\n" . mysqli_error(self::$MySQLConn) . "\n" . print_r($_REQUEST,true). print_r(debug_backtrace(),true) . print_r($_SERVER,true));
            }
         }
      else {
         self::$MySQLDbf = mysqli_select_db(self::$MySQLConn,Config::getConfig('MySQL/database'));
         if (!self::$MySQLDbf) {
            Logger::LogIt('Error: Unable to open target database.',Logger_ERRLVL_DEBUG,mysqliConn_Logger_Category);
            if(Config::getConfig('MySQL/emailerrorto')) {
               mail(Config::getConfig('MySQL/emailerrorto'),$_SERVER['HTTP_HOST'] . ' Unable to open target database(' . Config::getConfig('MySQL/database') . ')','PHP_SELF=' . $_SERVER['PHP_SELF'] . "\nURI=" . $_SERVER['REQUEST_URI'] . "\nFile=" . __file__ . "\nLine#=" . __line__ . "\nIP=" . $_SERVER['REMOTE_ADDR'] . "\n" . $_SERVER['HTTP_REFERER'] . "\n" . mysqli_error(self::$MySQLConn) . "\n" . print_r($_REQUEST,true). print_r(debug_backtrace(),true));
               }
            return(2);
            }
         else {
            if (!mysqli_set_charset(self::$MySQLConn,'utf8')) {
               Logger::LogIt('Error loading character set utf8: ' . mysqli_error(self::$MySQLConn),Logger_ERRLVL_DEBUG,mysqliConn_Logger_Category);
               }
            else {
               Logger::LogIt('Database connection established: ' . mysqli_character_set_name(self::$MySQLConn),Logger_ERRLVL_DEBUG,mysqliConn_Logger_Category);
               }
            }
         }
      }

    public static function LogQueries() {
        Logger::LogIt('Query logging enabled',Logger_ERRLVL_DEBUG,mysqliConn_Logger_Category);
        self::$LogQueries = true;
        }

   public static function RowsAffected() {
      return mysqli_affected_rows(self::$MySQLConn);
      }

   public static function RowsReturned() {
      return self::$lastRowsReturned;
      }

   public static function GetEscapedString($String) {
      if(!self::$MySQLConn) {
         self::connect();
         }
      return mysqli_real_escape_string(self::$MySQLConn,$String);
      }

   public static function GetLastInsertId() {
      return mysqli_insert_id(self::$MySQLConn);
      }

   public static function GetLastErrNo() {
      return mysqli_errno(self::$MySQLConn);
      }

   public static function GetLastErrMsg() {
      return mysqli_error(self::$MySQLConn);
      }

   public static function QueryJSON($SQL) {
      $dataArray = array();
      $jsonData = '';
      if (self::$LogQueries) {
         Logger::LogIt('QueryJSON: ' . $SQL,Logger_ERRLVL_DEBUG,mysqliConn_Logger_Category);
         }
      if(!self::$MySQLConn) {
         self::connect();
         }
      $Results = mysqli_query(self::$MySQLConn,$SQL);
      if (mysqli_errno(self::$MySQLConn)) {
         self::$lastRowsReturned = 0;
         if (self::$LogQueries) {
            Logger::LogIt(mysqli_errno(self::$MySQLConn) . ":" . mysqli_error(self::$MySQLConn),Logger_ERRLVL_DEBUG,mysqliConn_Logger_Category);
            }
         if(Config::getConfig('MySQL/emailerrorto')) {
            mail(Config::getConfig('MySQL/emailerrorto'),$_SERVER['HTTP_HOST'] . ' SQL Error','PHP_SELF=' . $_SERVER['PHP_SELF'] . "\nURI=" . $_SERVER['REQUEST_URI'] . "\nFile=" . __file__ . "\nLine#=" . __line__ . "\nIP=" . $_SERVER['REMOTE_ADDR'] . "\n" . $_SERVER['HTTP_REFERER'] . "\n" . $SQL . "\n" . mysqli_error(self::$MySQLConn) . "\n" . print_r($_REQUEST,true). print_r(debug_backtrace(),true));
            }
         }
      else {
         self::$lastRowsReturned = mysqli_num_rows($Results);
         while ( $rowData = mysqli_fetch_assoc($Results)) {
            $dataArray[] = $rowData;
            }
         $jsonData = json_encode($dataArray);
         }
      return $jsonData;
      }

    public static function Call($procedureName,$parameters,$returnType = '') {
        $dataArray = '';
        if(!self::$MySQLConn) {
            self::connect();
            }
        $procedureName = mysqli_real_escape_string(self::$MySQLConn,$procedureName);
        if($parameters) {
            $safeParams = array();
            foreach($parameters as $param) {
                $safeParams[] = mysqli_real_escape_string(self::$MySQLConn,$param);
                }
            $placeHolders = substr(str_repeat('?',count($parameters)),-1);
            $SQL = 'call '.$procedureName.'(\''.implode('\',\'',$safeParams).'\')';
            }
        else {
            $SQL = 'call '.$procedureName.'()';
            }
        if (self::$LogQueries) {
            Logger::LogIt('RunProcedure: ' . $SQL,Logger_ERRLVL_DEBUG,mysqliConn_Logger_Category);
            }

        $Results = mysqli_query(self::$MySQLConn,$SQL);
        if (mysqli_errno(self::$MySQLConn)) {
            if (self::$LogQueries) {
                Logger::LogIt(mysqli_errno(self::$MySQLConn) . ':' . mysqli_error(self::$MySQLConn),Logger_ERRLVL_DEBUG,mysqliConn_Logger_Category);
                }
            if(Config::getConfig('MySQL/emailerrorto')) {
                mail(Config::getConfig('MySQL/emailerrorto'),$_SERVER['HTTP_HOST'] . ' SQL Error','PHP_SELF=' . $_SERVER['PHP_SELF'] . "\nURI=" . $_SERVER['REQUEST_URI'] . "\nFile=" . __file__ . "\nLine#=" . __line__ . "\nIP=" . $_SERVER['REMOTE_ADDR'] . "\n" . $_SERVER['HTTP_REFERER'] . "\n" . $SQL . "\n" . mysqli_error(self::$MySQLConn) . "\n" . print_r($_REQUEST,true). print_r(debug_backtrace(),true));
                }
            }
        else {
            while($row = mysqli_fetch_assoc($Results)) {
                $dataArray[] = $row;
                mysqli_next_result(self::$MySQLConn);
                }
            mysqli_free_result($Results);
            }
        return $dataArray;
        }

    public static function getAESEncryptCode() {
        return Config::getConfig('MySQL/aesencryptcode');
        }

    // End of class
    }
