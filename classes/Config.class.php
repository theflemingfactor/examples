<?php

/*
 * Simple config class to read YML files and generate array.
 * Handles overriding by config files stored in sub directories and merges data.
 */
include_once(dirname(__FILE__) . '/Spyc.php');

class Config {
   private static $_CONFIG;


   public static function loadSystemConfig($configFile) {
      $yamlLoader = new Spyc();
      $config = array();
      if(file_exists($configFile)) {
         Logger::LogIt('Loading: ' . $configFile,Logger_ERRLVL_DEBUG);
         $config = $yamlLoader->YAMLLoad($configFile);
         }
      self::$_CONFIG = $config;
      }

   public static function loadConfig() {
      $yamlLoader = new Spyc();
      $startingPath = $_SERVER['DOCUMENT_ROOT'] . USER_BASE . '/config';
      Logger::LogIt('Starting path: ' . $startingPath,Logger_ERRLVL_DEBUG);
      $config = self::$_CONFIG;
      if(file_exists($startingPath . '/config.yml')) {
         Logger::LogIt('Loading: ' . $startingPath . '/config.yml',Logger_ERRLVL_DEBUG);
         $array = $yamlLoader->YAMLLoad($startingPath . '/config.yml');
         $config = self::array_merge_recursive_distinct($config,$array);
         }
      foreach(self::$_pathParts as $pathPart) {
         if($pathPart) {
            $startingPath .= '/' . $pathPart;
            Logger::LogIt($startingPath,NasPay_ERRLVL_DEBUG);
            if(file_exists($startingPath . '/config.yml')) {
               Logger::LogIt('Loading: ' . $startingPath . '/config.yml',Logger_ERRLVL_DEBUG);
               $array = $yamlLoader->YAMLLoad($startingPath . '/config.yml');
               $config = self::array_merge_recursive_distinct($config,$array);
               }
            }
         }
      self::$_CONFIG = $config;
      }

   public static function getConfig($configKey = '') {
      if($configKey) {
         $keyParts = explode('/',$configKey);
         $nextValue = self::$_CONFIG;
         foreach($keyParts as $keyElement) {
            if(isset($nextValue[$keyElement])) {
               $nextValue=$nextValue[$keyElement];
               }
            else {
               $nextValue = '';
               }
            }
         return $nextValue;
         }
      else {
         return self::$_CONFIG;
         }
      }

   private function array_merge_recursive_distinct ( array &$array1, array &$array2 ) {
      $merged = $array1;
      foreach ( $array2 as $key => &$value ) {
         if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) ) {
            $merged [$key] = self::array_merge_recursive_distinct ( $merged [$key], $value );
            }
         else {
            $merged [$key] = $value;
            }
         }
      return $merged;
      }
}