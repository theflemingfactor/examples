<?php

define('Logger_ERRLVL_CRITICALONLY',0);
define('Logger_ERRLVL_1',1);
define('Logger_ERRLVL_2',2);
define('Logger_ERRLVL_DEBUG',3);

date_default_timezone_set('America/Chicago');

class Logger {
    private static $_startTime;
    private static $_logLevel;
    private static $_AppName;
    private static $_LogDirectory = '';

    public static function Start($appName='',$loggingLevel = Logger_ERRLVL_1) {
        // Store out start time. This is used to monitor performance
        self::$_startTime = microtime(true);
        self::$_logLevel = $loggingLevel;
        self::$_AppName = $appName;
        self::LogIt('Starting...',Logger_ERRLVL_DEBUG);
    }

    public static function DataStart($loggingLevel = Logger_ERRLVL_1) {
        // Store out start time. This is used to monitor performance
        self::$_startTime = microtime(true);
        self::$_logLevel = $loggingLevel;
        self::LogIt('Starting Data Request...',Logger_ERRLVL_DEBUG);
    }

    public static function Finish() {
        self::LogIt('Execute Time: ' . sprintf("%0.4f",(microtime(true)-self::$_startTime) . 'secs',Logger_ERRLVL_DEBUG));
    }

    // Return the time since execution started
    public static function getExecuteTime() {
        self::LogIt('Execute Time: ' . (microtime(true)-self::$_startTime));
        return microtime(true)-self::$_startTime;
    }

    public static function setLogLevel($logLevel) {
        self::$_logLevel = $logLevel;
    }

   public static function setLogDirectory($logDirectory) {
        self::$_LogDirectory = $logDirectory . (substr($logDirectory,-1)=='/'?'':'/');
    }

    public static function LogIt($logMsg,$logLevel=Logger_ERRLVL_DEBUG,$logType='txt',$file='',$line='') {
        $file = basename($file);
        if($logLevel<=self::$_logLevel) {
            if(self::$_LogDirectory) {
                $logFile = self::$_LogDirectory . self::$_AppName . '_' . date('Y-m-d') . ($logType?'.':'') . $logType;
                $fh = fopen($logFile,'a');
                fwrite($fh,date('H:i:s') . ': ');
                if ($file && $line) {
                    fwrite($fh,$file . ' - ' . $line . ': ');
                }
                fwrite($fh,$logMsg . "\n");
                fclose($fh);
            }
            else {
                error_log(self::$_AppName . ': ' . $logMsg);
            }
        }
    }

}
